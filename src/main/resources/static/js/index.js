
function getUnreservedVehicles() {
    return sendHttpRequest('GET', '/vehicle/getUnreservedVehicles')
        .then(responseData => {
            return responseData;
        })
        .catch(err => {
            console.log(err);
        });
};

function getAllVehicles() {
    return sendHttpRequest('GET', '/vehicle/getAll')
        .then(responseData => {
            return responseData;
        })
        .catch(err => {
            console.log(err);
        });
};

function getVehiclesByData() {
    let condition = document.getElementById('textToFindEntry').value;
    return sendHttpRequest('GET', `/vehicle/getByData?data=${condition}`)
        .then(responseData => {
            return responseData;
        })
        .catch(err => {
            console.log(err);
        });
}

function getReservationByData() {
    let condition = document.getElementById('textToFindReserv').value;
    return sendHttpRequest('GET', `/reservation/getAllReservDTOByData?data=${condition}`)
        .then(responseData => {
            return responseData;
        })
        .catch(err => {
            console.log(err);
        });
}

function fillMainTable(listPromise, tableId, btnFilling, btnMethod) {
    let mainTable = document.getElementById(tableId);
    window.onload = clearTable(mainTable);
    //trzeba zwrócić "promise" za pomocą return, żeby później go kontynowować przy użyciu
    // np. pobierania danych z tabelki po pobraniu danych
    return listPromise.then((response) => {
        for (let i = 1; i < response.length + 1; i++) {
            let newRow = mainTable.insertRow(i);
            cell1 = newRow.insertCell(0);
            cell2 = newRow.insertCell(1);
            cell3 = newRow.insertCell(2);
            cell4 = newRow.insertCell(3);

            cell1.innerHTML = response[i - 1].registrationNumber;
            cell2.innerHTML = response[i - 1].type;
            cell3.innerHTML = response[i - 1].mark;

            let btn = document.createElement("button");
            btn.type = "button";
            btn.innerHTML = btnFilling;
            cell4.appendChild(btn);
            btn.addEventListener("click", () => {
            btnMethod(i); 
            });
        }
    });
}

function clearTable(table) {
    for (let i = table.rows.length - 1; i > 0; i--) {
        table.deleteRow(i);
    }
}

function pageRedirect(slash) {
    return window.location.replace(window.location + slash);
}

function getFreePlace() {
    return sendHttpRequest('GET', '/place/freePlace')
        .then(responseData => {
            return responseData;
        })
        .catch(err => {
            console.log(err);
        });
}

function actualDateTime() {
    let today = new Date();
    let date = today.getUTCFullYear() + '-' + (today.getUTCMonth() + 1) + '-' + today.getUTCDate();
    let time = today.getHours() + ':' + today.getMinutes() + ':' + today.getSeconds();
    let dateTime = (date + 'T' + time);
    return dateTime;
}

function addReservation(i) {
    // value odnosi się do elementów tekstowych a innerHTML obiektów typowo HTML
    let regNum = document.getElementById("listOfVehiclesIx").rows[i].cells[0].innerHTML;
    // przy @RequestParam w javie trzeba podawać ten parametr  w url w js
    sendHttpRequest('POST', `/reservation/add?registrationNumber=${regNum}`, {
        registrationNumber: regNum

        // korzystam z ()=> jako "callback", żeby tutaj wywołać całą funkcję a nie poprzez samo"fillRegDeP()" oczekiwać
        // tylko samego zwrotu, czyli w tym drugim przypadku funkcja wykonuje się wcześniej niż zamierza,
        // aby po "then" zwrócić już tylko wynik
    }).then(() => fillRegDepartureTable(getAllReservation(), 'regDeparture', 'Wyrejestruj'))
        .catch(err => {
            console.log(err);
        });
};
//napisać funkcję wywołujaca akcje po kliknięciu przycisku tabelki

function getReservedPlaceByRegNum(registrationNum) {
    return sendHttpRequest('GET', `/place/getReservPlaceByRegNum=${registrationNum}`)
        .then(responseData => {
            return responseData;
        })
        .catch(err => {
            console.log(err);
        });
}

function changeAvialability(placeNumber, avialability) {
    return sendHttpRequest('POST', '/place/')
        .then(responseData => {
            return responseData;
        })
        .catch(err => {
            console.log(err);
        });
}

function log() {
    console.log('okok');
}

function getAllReservation() {
    return sendHttpRequest('GET', '/reservation/getAllResrvDTO')
        .then(responseData => {
            return responseData;
        })
        .catch(err => {
            console.log(err);
        });
}

function deleteVehicle(i) {
    let table = document.getElementById('deleteVehicleTab');
        let regNumber;
            regNumber = table.rows[i].cells[0].innerHTML;
               table.deleteRow(i);
         return sendHttpRequest('DELETE', `/vehicle/delete?regNumber=${regNumber}`)
        .then(responseData =>{
            return responseData
        })
        .catch(err=>{
            console.log(err);
        });   
}


function fillRegDepartureTable(listPromise, tableId, btnFilling) {
    let mainTable = document.getElementById(tableId);
    window.onload = clearTable(mainTable);
    //trzeba zwrócić "promise" za pomocą return, żeby później go kontynowować przy użyciu
    // np. pobierania danych z tabelki po pobraniu danych
    return listPromise.then((response) => {
        for (let i = 1; i < response.length + 1; i++) {
            let newRow = mainTable.insertRow(i);
            cell1 = newRow.insertCell(0);
            cell2 = newRow.insertCell(1);
            cell3 = newRow.insertCell(2);
            cell4 = newRow.insertCell(3);
            cell5 = newRow.insertCell(4);
            cell6 = newRow.insertCell(5);

            cell1.innerHTML = response[i - 1].registrationNumber;
            cell2.innerHTML = response[i - 1].model;
            cell3.innerHTML = response[i - 1].type;
            cell4.innerHTML = response[i - 1].place;

            let text = response[i - 1].entryDate;
            text1 = text.substr(0, 10);
            text2 = text.substr(11, 5);
            cell5.innerHTML = text1 + ' ' + text2;

            let btn = document.createElement("button");
            btn.type = "button";
            btn.innerHTML = btnFilling;
            cell6.appendChild(btn);

            btn.addEventListener("click", () => {
            archiveReservation(response[i - 1].registrationNumber);
            mainTable.deleteRow(i);
            });

        }
    });
}

function addReservation(i) {
    // value odnosi się do elementów tekstowych a innerHTML obiektów typowo HTML
    let regNum = document.getElementById("listOfVehiclesIx").rows[i].cells[0].innerHTML;
    // przy @RequestParam w javie trzeba podawać ten parametr  w url w js
    return sendHttpRequest('POST', `/reservation/add?registrationNumber=${regNum}`, {
        registrationNumber: regNum

        // korzystam z ()=> jako "callback", żeby tutaj wywołać całą funkcję a nie poprzez samo"fillRegDeP()" oczekiwać
        // tylko samego zwrotu, czyli w tym drugim przypadku funkcja wykonuje się wcześniej niż zamierza,
        // aby po "then" zwrócić już tylko wynik
    }).then(() => fillRegDepartureTable(getAllReservation(), 'regDeparture', 'Wyrejestruj'))
        .then(() => fillMainTable(getUnreservedVehicles(),'listOfVehiclesIx','Zarejestuj', addReservation))
        .catch(err => {
            console.log(err);
        });
    }

    function archiveReservation(regNum) {
        return sendHttpRequest('POST', `/reservation/archive?registrationNumber=${regNum}`, {
            registrationNumber : regNum
        }).then(() => fillRegDepartureTable(getAllReservation(), 'regDeparture', 'Wyrejestruj'))
            .then(()=>  fillMainTable(getUnreservedVehicles(),'listOfVehiclesIx','Zarejestuj', addReservation))
            .catch(err => {
                console.log(err);
            });
    }

    function getAvialablePlaces(){
        return sendHttpRequest('GET', `/place/avialablePlaces`) 
        .then(responseData => {
            return responseData;
        })
        .catch(err => {
            console.log(err);
        });
    }

    function fillAllAvialablePlaces(){
        let table = document.getElementById("freePlacesTab");
        return getAvialablePlaces().then((response)=>{
            for(let i=1 ; i < response.length + 1 ; i++) {
                let newRow = table.insertRow(i);
                     cell1 = newRow.insertCell(0);
                     cell1.innerHTML = response[i-1];
            }
        });
    }

   // function reloadWindow() {
      //  setTimeOut(window.location.reload(true), 30000);
   // }
        //for (var i; i < document.getElementById("table").length; i++){
           // if(table.)
       // }



	// const getAllVeh = document.getElementById('freePlacesIx');
	// getAllVeh.addEventListener('click', fillMainTable());

