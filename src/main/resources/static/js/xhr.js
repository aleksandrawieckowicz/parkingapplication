const sendHttpRequest = (method, url, data) => {
    const promise = new Promise((resolve, reject) =>{
    const xhr = new XMLHttpRequest();
    xhr.open(method, url);
    
 //to formatuje jsona na js-object
xhr.responseType = 'json';

if(data){
xhr.setRequestHeader('Content-Type', 'application/json');
}

//"=()=>" powoduje, że this nie odnosi się do xhr.onload lecz do główniej funkcji "rodzica" czyli
// to nie to samo co xhr.onload = function()
    xhr.onload = () => {
        if (xhr.status >=400){
            reject(xhr.response);
        } else {
            resolve(xhr.response);
        }
    };

    xhr.onerror = () =>{
        reject('Something went wrong');
    };

    xhr.send(JSON.stringify(data));
    console.log("ok");
});
    return promise;
}

 



