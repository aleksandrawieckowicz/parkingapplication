const addNewVehicle = () => {
    // value odnosi się do elementów tekstowych a innerHTML obiektów typowo HTML
    regNum = document.getElementById('regNumAddNewVeh').value;
    mark = document.getElementById('markAddNewVeh').value;
    type = document.getElementById('typeAddNewVeh').value;
    sendHttpRequest('POST', '/vehicle/add', {
    registrationNumber: regNum,
    mark: mark,
    type: type  
}).then(responseData => {
    console.log(responseData)
})
.catch(err=> {
    console.log(err);
});
};

//ta funkcja czeka aż cała strona się załaduje a póżniej pobiera przycisk
window.onload = function() {
	const addVeh = document.getElementById('addNewVehicle');
    addVeh.addEventListener('click', addNewVehicle);
    
};