package com.mycompany.parking.dto;

import java.util.Date;

public class ReservationDTO {
	
	String registrationNumber;
	String model;
	String type;
	int place;
	Date entryDate;
	
	public ReservationDTO(String registrationNumber, String model, String type, int place, Date entryDate) {
		super();
		this.registrationNumber = registrationNumber;
		this.model = model;
		this.type = type;
		this.place = place;
		this.entryDate = entryDate;
	}
	

	public String getRegistrationNumber() {
		return registrationNumber;
	}
	public void setRegistrationNumber(String registrationNumber) {
		this.registrationNumber = registrationNumber;
	}
	public String getModel() {
		return model;
	}
	public void setModel(String model) {
		this.model = model;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public int getPlace() {
		return place;
	}
	public void setPlace(int place) {
		this.place = place;
	}
	public Date getEntryDate() {
		return entryDate;
	}
	public void setEntryDate(Date entryDate) {
		this.entryDate = entryDate;
	}
	
	
	
	
	
}
