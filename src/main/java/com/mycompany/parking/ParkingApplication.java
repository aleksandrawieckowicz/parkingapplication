package com.mycompany.parking;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;

import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

import com.mycompany.parking.model.Vehicle;
import com.mycompany.parking.repository.jdbc.JdbcVehicleRepository;

@SpringBootApplication
@ComponentScan(basePackages= {"com.mycompany.parking"})
public class ParkingApplication {

	
	public static void main(String[] args) {
	SpringApplication.run(ParkingApplication.class, args);
	
	
	}
	

	
	
	
	

}
