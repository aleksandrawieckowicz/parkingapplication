package com.mycompany.parking.repository.jdbc;

import java.util.List;

import com.mycompany.parking.dto.ReservationDTO;
import com.mycompany.parking.model.Reservation;


public interface ReservationRepository {

	public void addReservation(String entryDate, int placeNumber, String registrationNumber);
	
	public void deleteReservation(String regNumber);
	
	public Reservation getReservationByPlace(int placeNumber);
	
	public Reservation getReservationByRegNumer(String regNumber);
	
	public double getDailyCharges();
	
	public List<ReservationDTO> getAllReservationDTO();
	
	public List<ReservationDTO> getAllReservationDTOByData(String data);
	
	
	
	
}
