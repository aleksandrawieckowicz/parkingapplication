package com.mycompany.parking.repository.jdbc;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

@Repository
public class JdbcArchivalReservationRepository implements ArchivalReservationRepository {
	
	@Autowired
	JdbcTemplate jdbcTemplate;

	@Override
	public void archiveReservation(String registrationNumber, String departureDate) {
		jdbcTemplate.update("INSERT INTO archivalReservation (entryDate, placeNumber, registrationNumber, departureDate) " + 
				"SELECT reservation.entryDate, reservation.placeNumber, reservation.registrationNumber, ? as departureDate " + 
				"FROM reservation WHERE registrationNumber = ?", departureDate, registrationNumber); 
	}
}
