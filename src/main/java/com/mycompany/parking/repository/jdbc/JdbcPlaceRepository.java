package com.mycompany.parking.repository.jdbc;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import com.mycompany.parking.model.Place;

@Repository
public class JdbcPlaceRepository implements PlaceRepository {
	
	@Autowired
	JdbcTemplate jdbcTemplate;

	@Override
	public void addPlace(Place place) {
		jdbcTemplate.update("INSERT INTO place (placeNumber, avialability) values (?,?)", place.getNumber(), place.getAvialability());
	}

	@Override
	public void deletePlace(int placeNumber) {
		jdbcTemplate.update("DELETE FROM place WHERE placeNumber = ?", placeNumber);
	}


	@Override
	public int getFreePlace() {
		return jdbcTemplate.queryForObject("SELECT placeNumber FROM place WHERE avialability = 'dostępne' ORDER BY placeNumber ASC LIMIT 1",
				(rs, rowNum) -> (rs.getInt("placeNumber"))
				);
	}
	
	@Override
	public int getReservedPlaceByRegNum(String regNum) {
		return jdbcTemplate.queryForObject("SELECT placeNumber FROM reservation WHERE registrationNumber = ?", (rs, rowNum) -> (rs.getInt("placeNumber")) ,regNum);
		
	}

	@Override
	public void makePlaceAvailable(int placeNumber) {
		jdbcTemplate.update("UPDATE place SET avialability = 'dostępne' WHERE placeNumber = ?", placeNumber);
	}

	@Override
	public void makePlaceUnavailable(int placeNumber) {
		  jdbcTemplate.update("UPDATE place SET avialability = 'niedostępne' WHERE placeNumber = ?", placeNumber);
	}

	@Override
	public List<Integer> getAvialablePlaces() {
		return jdbcTemplate.query("SELECT placeNumber FROM place WHERE avialability = 'dostępne'", (rs, rowNum) -> (rs.getInt("placeNumber")));	
	}
}
