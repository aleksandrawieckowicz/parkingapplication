package com.mycompany.parking.repository.jdbc;

import java.util.List;

import com.mycompany.parking.model.Type;

public interface TypeRepository {

	int addType(Type type);
	
	int deleteType(String typeName);
	
	int changePrice(String vehicleType, double price);
	
	List<Type> getAllTypes(); 
}
