package com.mycompany.parking.repository.jdbc;

import java.util.List;

import com.mycompany.parking.model.Place;

public interface PlaceRepository {
	
	public void addPlace(Place place);
	
	public void deletePlace(int placeNumber);
	
	public void makePlaceAvailable(int placeNumber);
	
	public void makePlaceUnavailable(int placeNumber);
	
	public int getFreePlace();
	
	public int getReservedPlaceByRegNum(String regNum);
	
	public List<Integer> getAvialablePlaces();
	

}
