package com.mycompany.parking.repository.jdbc;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import com.mycompany.parking.model.Vehicle;
import com.mycompany.parking.model.Type;

@Repository
public class JdbcVehicleRepository implements VehicleRepository {

	@Autowired
	private JdbcTemplate jdbcTemplate;

	@Override
	public int addNewVehicle(Vehicle vehicle) {
		return jdbcTemplate.update("INSERT INTO vehicle (registrationNumber, mark, vehicleType) values (?,?,?)",
				vehicle.getRegistrationNumber(), vehicle.getMark(), vehicle.getType());

	}

	@Override
	public int deleteVehicle(String regNumber) {
		return jdbcTemplate.update("DELETE FROM vehicle WHERE registrationNumber = ?", regNumber);
	}

	// trzeba obsłużyć "EmptyResultDataAccessException", ponieważ
	// queryForObject(zwraca pojedyncze rekordy)
	// w przypadku niezwrócenia żadnej wartości zwraca go z komentarzem: Incorrect
	// result size: expected 1, actual 0
	@Override
	public List<Vehicle> getVehicleByData(String data) {
		try {
			return jdbcTemplate.query(
					"SELECT * FROM vehicle WHERE registrationNumber LIKE '%" + data + "%' OR mark LIKE '%" + data
							+ "%'",
					(rs, rowNum) -> (new Vehicle(rs.getString("registrationNumber"), rs.getString("mark"),
							rs.getString("vehicleType"))));
		} catch (EmptyResultDataAccessException e) {
			return null;
		}
	}

	@Override
	public List<Vehicle> getAllVehicle() {
		return jdbcTemplate.query("SELECT * FROM vehicle",
				(rs, rowNum) -> (new Vehicle(rs.getString("registrationNumber"), rs.getString("mark"),
						rs.getString("vehicleType"))));
	}

	@Override
	public List<Vehicle> getUnreservedVehicles(){
		return jdbcTemplate.query("SELECT * FROM vehicle WHERE NOT EXISTS "
				+ "(SELECT NULL FROM reservation WHERE vehicle.registrationNumber = reservation.registrationNumber)",
				(rs, rowNum) -> new Vehicle(rs.getString("registrationNumber"), rs.getString("mark"), 
				rs.getString("vehicleType")));	
		}
}
