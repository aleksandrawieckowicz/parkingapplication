package com.mycompany.parking.repository.jdbc;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import com.mycompany.parking.model.Type;

@Repository
public class JdbcTypeRepository implements TypeRepository{

	@Autowired
	JdbcTemplate jdbcTemplate;
	
	@Override
	public int addType(Type type) {
		return jdbcTemplate.update("INSERT INTO type (typeName, pricePerHour) VALUES (?,?)", type.getvehicleType(), type.getPricePerHour());
	}

	@Override
	public int deleteType(String vehicleType) {
		return jdbcTemplate.update("DELETE FROM type WHERE vehicleType =?",vehicleType);
	}

	@Override
	public int changePrice(String typeName, double price) {
		return jdbcTemplate.update("UPDATE type SET pricePerHour = ? WHERE vehicleType = ?", price, typeName);
	}

	//query nie zwraca pojedynczych obiektów, zwraca np. listy
	@Override
	public List<Type> getAllTypes() {
		return jdbcTemplate.query("SELECT * FROM type", (rs, rowNum) ->
			new Type(
					rs.getString("vehicleType"),
					rs.getDouble("pricePerHour")
					)
				);
	}

	
}
