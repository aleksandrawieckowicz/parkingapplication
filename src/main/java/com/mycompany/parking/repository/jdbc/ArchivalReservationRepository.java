package com.mycompany.parking.repository.jdbc;

public interface ArchivalReservationRepository {

	public void archiveReservation(String registrationNumber, String departureDate);
	
}
