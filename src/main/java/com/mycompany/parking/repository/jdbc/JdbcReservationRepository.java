 package com.mycompany.parking.repository.jdbc;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import com.mycompany.parking.dto.ReservationDTO;
import com.mycompany.parking.model.Reservation;

@Repository
public class JdbcReservationRepository implements ReservationRepository{
	
	@Autowired
	JdbcTemplate jdbcTemplate;

	@Override
	public void deleteReservation(String regNumber) {
		   jdbcTemplate.update("DELETE FROM reservation WHERE registrationNumber = ?", regNumber);
	}

	@Override
	public Reservation getReservationByPlace(int placeNumber) {
		return jdbcTemplate.queryForObject("SELECT * FROM reservation WHERE placeNumber = ?", (rs, rowNum) -> 
		(new Reservation(rs.getInt("id"), rs.getDate("entryDate"), rs.getInt("placeNumber"), rs.getString("registrationNumber"))) , placeNumber);
	}

	@Override
	public Reservation getReservationByRegNumer(String regNumber) {
		return jdbcTemplate.queryForObject("SELECT * FROM reservation WHERE registrationNumber = ?", (rs, rowNum) -> 
		(new Reservation(rs.getInt("id"), rs.getDate("entryDate"), rs.getInt("placeNumber"), rs.getString("registrationNumber"))), regNumber);
	}

	// to będzie w archiwalnych rezerwacjach, będzie się szukało po dacie
	@Override
	public double getDailyCharges() {
		return jdbcTemplate.queryForObject("SELECT SUM(charge) sumCharge FROM reservation", (rs, rowNum) -> rs.getDouble("sumCharge"));
	}

	@Override
	public void addReservation(String entryDate, int placeNumber, String registrationNumber) {
		   jdbcTemplate.update("INSERT INTO reservation (entryDate, placeNumber, registrationNumber) VALUES (?,?,?)", entryDate
				, placeNumber , registrationNumber);
	}

	@Override
	public List<ReservationDTO> getAllReservationDTO() {
		return jdbcTemplate.query("SELECT reservation.entryDate, reservation.placeNumber, reservation.registrationNumber,"
				+ " vehicle.vehicleType, vehicle.mark FROM reservation INNER JOIN vehicle"
				+ " ON reservation.registrationNumber = vehicle.registrationNumber", (rs, rowNum)-> 
				new ReservationDTO(rs.getString("registrationNumber"), rs.getString("mark"), rs.getString("vehicleType"), 
				rs.getInt("placeNumber"), new Date(rs.getTimestamp("entryDate").getTime())));
	}

	@Override
	public List<ReservationDTO> getAllReservationDTOByData(String data) {
		return jdbcTemplate.query("SELECT reservation.entryDate, reservation.placeNumber, reservation.registrationNumber, vehicle.vehicleType, vehicle.mark"  
				+ " FROM reservation INNER JOIN vehicle ON reservation.registrationNumber = vehicle.registrationNumber"
				+ " WHERE vehicle.registrationNumber LIKE '%"+ data + "%' OR mark LIKE '%"+ data +"%'", (rs, rowNum)->
				new ReservationDTO(rs.getString("registrationNumber"), rs.getString("mark"), rs.getString("vehicleType"),
						rs.getInt("placeNumber"), new Date(rs.getTimestamp("entryDate").getTime())));
	}
}
