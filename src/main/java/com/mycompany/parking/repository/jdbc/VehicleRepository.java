package com.mycompany.parking.repository.jdbc;

import java.util.List;
import java.util.Optional;

import com.mycompany.parking.model.Vehicle;

public interface VehicleRepository {

	
	int addNewVehicle (Vehicle vehicle);
	
	int deleteVehicle(String regNumber);
	
	List<Vehicle> getVehicleByData(String data);
	
	List<Vehicle> getAllVehicle();
	
	List<Vehicle> getUnreservedVehicles();
	
	
}
