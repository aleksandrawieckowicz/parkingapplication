package com.mycompany.parking.model;

import java.util.Date;

public class Reservation {

	int id;
	Date entryDate;
	int placeNumber;
	String registrationNumber;

	
	public Reservation(int id, Date entryDate, int placeNumber, String registrationNumber) {
		
		super();
		this.id = id;
		this.entryDate = entryDate;
		this.placeNumber = placeNumber;
		this.registrationNumber = registrationNumber;
	}
	
	public int getId() {
		return id;
	}
	
	public void setId(int id) {
		this.id = id;
	}

	public Date getEntryDate() {
		return entryDate;
	}

	// możliwe, że trzeba będzie użyć parse date , żeby pobrać w formacie yyyy-mm-dd hh-mm-ss
	// https://www.journaldev.com/17899/java-simpledateformat-java-date-format
	public void setEntryDate(Date entryDate) {
		this.entryDate = entryDate;
	}

	public int getPlaceNumber() {
		return placeNumber;
	}

	public void setPlaceNumber(int placeNumber) {
		this.placeNumber = placeNumber;
	}

	public String getRegistrationNumber() {
		return registrationNumber;
	}

	public void setRegistrationNumber(String registrationNumber) {
		this.registrationNumber = registrationNumber;
	}
	
}
