package com.mycompany.parking.model;

public class Place {

	int number;
	String avialability;
	
	public Place(int number, String avialability) {
		super();
		this.number = number;
		this.avialability = avialability;
	}
	public int getNumber() {
		return number;
	}
	public void setNumber(int number) {
		this.number = number;
	}
	public String getAvialability() {
		return avialability;
	}
	public void setAvialability(String avialability) {
		this.avialability = avialability;
	}
	
	
}
