package com.mycompany.parking.model;

import org.springframework.stereotype.Service;


public class Vehicle {
	
	private String registrationNumber;
	private String mark;
	private String type;
	
	public Vehicle() {
	}

	public Vehicle(String registrationNumber, String mark, String type) {
	
		this.registrationNumber = registrationNumber;
		this.mark = mark;
		this.type = type;
	}

	public String getRegistrationNumber() {
		return registrationNumber;
	}

	public String getMark() {
		return mark;
	}

	public void setMark(String mark) {
		this.mark = mark;
	}

	public void setRegistrationNumber(String registrationNumber) {
		this.registrationNumber = registrationNumber;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}
		
}
