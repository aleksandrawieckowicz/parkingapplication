package com.mycompany.parking.model;

public class Type {

	private String vehicleType;
	private double pricePerHour;

	public Type(String vehicleType, double pricePerHour) {
		super();
		this.vehicleType = vehicleType;
		this.pricePerHour = pricePerHour;
	}

	public String getvehicleType() {
		return vehicleType;
	}

	public void setType(String vehicleType) {
		this.vehicleType = vehicleType;
	}

	public double getPricePerHour() {
		return pricePerHour;
	}

	public void setPricePerHour(double pricePerHour) {
		this.pricePerHour = pricePerHour;
	}

}
