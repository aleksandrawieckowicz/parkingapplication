package com.mycompany.parking.model;

import java.util.Date;

public class ArchivalReservation {
	
	int id;
	Date entryDate;
	int placeNumber;
	String registrationNumber;
	Date departureDate;
	
	public ArchivalReservation(int id, Date entryDate, int placeNumber, String registrationNumber, Date departureDate) {
		super();
		this.id = id;
		this.entryDate = entryDate;
		this.placeNumber = placeNumber;
		this.registrationNumber = registrationNumber;
		this.departureDate = departureDate;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public Date getEntryDate() {
		return entryDate;
	}

	public void setEntryDate(Date entryDate) {
		this.entryDate = entryDate;
	}

	public int getPlaceNumber() {
		return placeNumber;
	}

	public void setPlaceNumber(int placeNumber) {
		this.placeNumber = placeNumber;
	}

	public String getRegistrationNumber() {
		return registrationNumber;
	}

	public void setRegistrationNumber(String registrationNumber) {
		this.registrationNumber = registrationNumber;
	}

	public Date getDepartureDate() {
		return departureDate;
	}

	public void setDepartureDate(Date departureDate) {
		this.departureDate = departureDate;
	}
	
	
	
	

}
