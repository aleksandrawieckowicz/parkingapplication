package com.mycompany.parking.service;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;

import com.mycompany.parking.model.Reservation;
import com.mycompany.parking.repository.jdbc.ArchivalReservationRepository;
import com.mycompany.parking.repository.jdbc.PlaceRepository;
import com.mycompany.parking.repository.jdbc.ReservationRepository;


@Service
public class ReservationServiceImpl implements ReservationService{
	
	@Autowired
	PlaceRepository placeRepository;
	
	@Autowired
	ReservationRepository reservationRepository;
	
	@Autowired
	ArchivalReservationRepository archivalReservationRepository;
	
	private String getActualDate() {
		DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
		LocalDateTime date = LocalDateTime.now();
		return dtf.format(date);
	}

	@Override
	@Transactional // jeśli któreś  z zapytań do BD się nie wykona to baza wraca do stanu sprzed wywołania
	public void addReservation(String registrationNumber) {
		int placeNumber = placeRepository.getFreePlace();
		reservationRepository.addReservation(getActualDate(), placeNumber, registrationNumber);
		placeRepository.makePlaceUnavailable(placeNumber);
	}

	@Override
	@Transactional
	public void archiveReservation(String registrationNumber) {
		int placeNumber = placeRepository.getReservedPlaceByRegNum(registrationNumber);
		archivalReservationRepository.archiveReservation(registrationNumber, getActualDate());
		reservationRepository.deleteReservation(registrationNumber);
		placeRepository.makePlaceAvailable(placeNumber);
	}
	
	// todo add reservarion
	// tutaj robisz cała logikę czyli pobierasz sobie jdbc. getfreeplace, generujesz godzinę i na końcu 
	// jdbc add reservation (to które masz teraz w kontrolerze)
	// to się nazywa tzw logika biznwsowa czyli przetworzenie całego żądania które może się składać z kilku odwolań
	// do bazy i jeszcze innej logiki takiej jak wygenerowanie godziny
	
	
}
