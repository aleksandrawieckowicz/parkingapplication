package com.mycompany.parking.service;

public interface ReservationService {

	void addReservation(String registrationNumber);
	
	void archiveReservation(String registrationNumber);

	
}
