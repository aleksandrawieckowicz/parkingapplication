package com.mycompany.parking.controller.redirect;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/")
public class HomeController {

	@GetMapping("/")
	public String home() {
		return "index";
	}
	
	@GetMapping("/addVehicle")
	public String addVehicle() {
		return "addVehicle";
	}
	
	@GetMapping("/deleteVehicle")
	public String deleteVehicle() {
		return "deleteVehicle";
	}
	
	@GetMapping("/freePlaces")
	public String freePlaces() {
		return "freePlaces";
	}
		
}
