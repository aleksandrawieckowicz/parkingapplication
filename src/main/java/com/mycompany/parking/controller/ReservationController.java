package com.mycompany.parking.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.mycompany.parking.dto.ReservationDTO;
import com.mycompany.parking.model.Reservation;
import com.mycompany.parking.repository.jdbc.ReservationRepository;
import com.mycompany.parking.service.ReservationService;

@RestController 
@RequestMapping("/reservation")
public class ReservationController {

	@Autowired
	ReservationRepository reservationRepository;
	@Autowired
	ReservationService reservationService;
	
	@PostMapping("/add")
	public void add(@RequestParam("registrationNumber") String registrationNumber) {
		reservationService.addReservation(registrationNumber);
	}
	
	@DeleteMapping("/delete")
	public void delete(String regNum) {
		reservationRepository.deleteReservation(regNum);
	}
	
	
	public Reservation getReservationByRegNumber(String regNumber) {
		return reservationRepository.getReservationByRegNumer(regNumber);
	}
		
	@GetMapping("/getByRegNumber")
	public Reservation getByRegNumber(@RequestParam("registrationNumber") String regNumber) {
		return reservationRepository.getReservationByRegNumer(regNumber);
	}
	
	@GetMapping("/getDailyCharges")
	public double getDailyCharges() {
		return reservationRepository.getDailyCharges();
	}
	
	@GetMapping("/getAllResrvDTO")
	public List<ReservationDTO> getAllReservatioDTO(){
		return reservationRepository.getAllReservationDTO();
	}
	
	@GetMapping("/getAllReservDTOByData")
	public List<ReservationDTO> getAllReservationDTOByData(@RequestParam("data") String data){
		return reservationRepository.getAllReservationDTOByData(data);
	}
	
	@PostMapping("/archive")
	public void archiveReservation(@RequestParam("registrationNumber") String registrationNumber){
		reservationService.archiveReservation(registrationNumber);
	}
}
