package com.mycompany.parking.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.mycompany.parking.model.Type;
import com.mycompany.parking.repository.jdbc.TypeRepository;

@RestController
@RequestMapping("/type")
public class TypeController {

	@Autowired
	TypeRepository typeRepository;

	@PostMapping("/add")
	public void add(@RequestBody Type type) {
		typeRepository.addType(type);
	}

	@DeleteMapping("/delete")
	public void delete(String vehicleType) {
		typeRepository.deleteType(vehicleType);
	}

	@PostMapping("/changePrice")
	public void changePriceperHour(String vehicleType, double price) {
		typeRepository.changePrice(vehicleType, price);
	}
	
	@GetMapping("/getAll")
	public List<Type> getAll(){
		return typeRepository.getAllTypes();
	}
}