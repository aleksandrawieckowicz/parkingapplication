package com.mycompany.parking.controller;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.mycompany.parking.model.Vehicle;
import com.mycompany.parking.repository.jdbc.VehicleRepository;

@RestController
@RequestMapping("/vehicle")
public class VehicleController {

	@Autowired
	VehicleRepository vehicleRepository;

	@PostMapping("/add")
	public void addVehicle(@RequestBody Vehicle vehicle) {
		vehicleRepository.addNewVehicle(vehicle);
	}

	@DeleteMapping("/delete")
	public void deleteVehicle(@RequestParam("regNumber") String regNumber) {
		vehicleRepository.deleteVehicle(regNumber);
	}

	// Jeśli warunek może zwrócić kilka obiektów to metoda musi być typu List<>
	@GetMapping("/getByData")
	public List<Vehicle> getVehicleByData(@RequestParam("data") String data) {
		return vehicleRepository.getVehicleByData(data);
	}

	@GetMapping("/getAll")
	public List<Vehicle> getAllVehicles() {
		return vehicleRepository.getAllVehicle();
	}

	@GetMapping("/getUnreservedVehicles")
	public List<Vehicle> getUnreservedVehicles() {
		return vehicleRepository.getUnreservedVehicles();
	}
}
