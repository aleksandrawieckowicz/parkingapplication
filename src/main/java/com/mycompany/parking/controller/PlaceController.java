package com.mycompany.parking.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.mycompany.parking.model.Place;
import com.mycompany.parking.repository.jdbc.PlaceRepository;

@RestController
@RequestMapping("/place")
public class PlaceController {
	
	@Autowired
	PlaceRepository placeRepository;
	
	@PostMapping("/add")
	public void add(@RequestBody Place place) {
		placeRepository.addPlace(place);
	}
	
	@DeleteMapping("/delete")
	public void delete(int placeNumber) {
		placeRepository.deletePlace(placeNumber);
	}
	
	@PostMapping("/makeAvialable")
	public void makePlaceAvailable(@RequestParam("placeNumber") int placeNumber) {
		placeRepository.makePlaceAvailable(placeNumber);
	}
	
	@PostMapping("/makeUnavialable")
	public void makePlaceUnavailable(@RequestParam("placeNumber") int placeNumber) {
		placeRepository.makePlaceUnavailable(placeNumber);
	}
	
	@GetMapping("/getReservPlaceByRegNum")
	public int getReservedPlaceByRegNum(@RequestParam("regNum") String regNum) {
		return placeRepository.getReservedPlaceByRegNum(regNum);
	}
	
	@GetMapping("/hello")
	public String hello() {
		return "hello";
	}
	
	@GetMapping("/avialablePlaces")
	public  List<Integer> getAvialablePlaces() {
		return placeRepository.getAvialablePlaces();
	}
}
