CREATE DATABASE parking
USE parking

CREATE TABLE PLACE (
placeNumber int,
avialability varchar(255)
);

ALTER TABLE place MODIFY placeNumber int NOT NULL AUTO_INCREMENT;

CREATE TABLE VEHICLE (
registrationNumber VARCHAR(255),
mark VARCHAR(255),
vehicleType VARCHAR(255)
);

CREATE TABLE TYPE (
typeName VARCHAR(255),
pricePerHour DOUBLE
);

ALTER TABLE PLACE
ADD PRIMARY KEY (placeNumber)

ALTER TABLE VEHICLE
ADD PRIMARY KEY (registrationNumber)

ALTER TABLE TYPE
ADD PRIMARY KEY (typeName)


CREATE TABLE RESERVATION (
id int,
entryDate datetime,
departureDate datetime,
placeNumber int,
registrationNumber VARCHAR(255),
PRIMARY KEY (id),
FOREIGN KEY (placeNumber) REFERENCES PLACE (placeNumber),
FOREIGN KEY (registrationNumber) REFERENCES VEHICLE (registrationNumber)
);


SELECT * FROM reservation
Insert into place values (3,"dostępny")

DELETE from place WHERE placeNummber 

DELETE From place where placeNumber = 11

 

LOCK TABLES
	place write,
    reservation write;

UNLOCK tables;

ALTER TABLE  reservation
DROP FOREIGN KEY  reservation_ibfk_3;
ALTER TABLE PLACE MODIFY placeNumber int;
ALTER TABLE place MODIFY placeNumber int auto_increment = 1;
DELETE FROM place where placeNumber = 5;
ALTER TABLE reservation ADD FOREIGN KEY (placeNumber) 
REFERENCES place (placeNumber);
SHOW CREATE TABLE reservation;
ALTER TABLE reservation MODIFY COLUMN placeNumber int;

SELECT * from place;
ALTER TABLE  reservation
DROP FOREIGN KEY  reservation_ibfk_2;
ALTER TABLE reservation ADD FOREIGN KEY (registrationNumber)
REFERENCES vehicle (registrationNumber);
ALTER TABLE reservation MODIFY id int auto_increment;
ALTER TABLE reservation auto_increment=1;
DELETE from place WHERE placeNumber>=1;

SHOW CREATE TABLE vehicle;

ALTER TABLE vehicle ADD FOREIGN KEY (vehicleType)
REFERENCES type (typeName);

ALTER TABLE place auto_increment=1;

SHOW CREATE TABLE vehicle;
SHOW CREATE TABLE type;
SELECT * FROM place
SELECT * FROM vehicle
INSERT INTO place VALUES (1, "dostępny")
SELECT * FROM reservation
INSERT INTO reservation VALUES (2, "2009-12-13", "2009-12-14", 1, "TKI3", 4.8)
ALTER TABLE reservation ADD COLUMN charge DOUBLE;
DELETE FROM type;
INSERT INTO type VALUES ("osobowy", 0.5);
SELECT * FROM vehicle WHERE registrationNumber LIKE '%lt%' OR mark LIKE '%TK%';
SELECT * FROM vehicle WHERE registrationNumber LIKE '%tk%' OR mark LIKE '%tk%'
INSERT INTO VEHICLE values ('U788878', 'Ford', 'osobowy')
SELECT * FROM vehicle

DELETE from place where placeNumber = 8



SELECT placeNumber FROM place WHERE avialability = "dostępny" ORDER BY placeNumber ASC LIMIT 1

SHOW CREATE TABLE reservation

ALTER TABLE place AUTO_INCREMENT = 1

INSERT INTO place (avialability) VALUES ("dostępny")
SELECT * FROM place
DELETE FROM place where placeNumber > 3
SELECT * FROM reservation
DELETE FROM reservation WHERE id >=1
ALTER TABLE reservation ADD UNIQUE (placeNumber, registrationNumber)
INSERT INTO reservation (entryDate, placeNumber, registrationNumber)
VALUES ('2019:12:19 18:54:34', 1, 'TKI54893')
SHOW CREATE TABLE RESERVATION

ALTER TABLE reservation DROP COLUMN charge

SELECT reservation.entryDate, reservation.placeNumber, reservation.registrationNumber, 
vehicle.vehicleType, vehicle.mark
FROM reservation INNER JOIN vehicle 
ON reservation.registrationNumber = vehicle.registrationNumber

 SELECT * FROM vehicle WHERE NOT EXISTS
(SELECT null FROM reservation WHERE vehicle.registrationNumber = reservation.registrationNumber)

CREATE table archivalReservation (
id int,
entryDate datetime,
placeNumber int,
registrationNumber varchar(255),
departureDate datetime 
)

SELECT * FROM reservation
SELECT * FROM vehicle
SELECT * FROM archivalReservation;
SHOW CREATE TABLE reservation

SELECT * FROM place



SELECT placeNumber FROM reservation WHERE registrationNumber = 'TKI54893';

UPDATE place SET avialability = 'dostępne' WHERE placeNumber = 10

INSERT INTO vehicle VALUES ("49", "Honda Civic", "osobowy");
INSERT into place values (26, "dostępne")

DELETE FROM reservation WHERE placeNumber = 1

SHOW CREATE TABLE archivalReservation
ALTER TABLE archivalReservation MODIFY id int NOT NULL AUTO_INCREMENT;

ALTER TABLE archivalReservation ADD PRIMARY KEY (id);

SELECT * FROM archivalReservation

INSERT INTO archivalReservation (entryDate, placeNumber, registrationNumber, departureDate)
SELECT reservation.entryDate, reservation.placeNumber, reservation.registrationNumber, "2009-12-13" as departureDate
FROM reservation WHERE registrationNumber = 'T5565554'  



UPDATE archivalReservation SET departureDate = "2009-12-13" WHERE registrationNumber = 'T775554'

DELETE FROM reservation WHERE registrationNumber = 'T775554'



SELECT * FROM reservation

SELECT * FROM archivalReservation

DELETE  FROM archivalReservation WHERE id = 1

ALTER TABLE archivalReservation ADD FOREIGN KEY (registrationNumber) 
REFERENCES reservation (registrationNumber);

SHOW create table archivalReservation

SHOW create table reservation
SELECT * FROM PLACE
ALTER TABLE  archivalReservation
DROP FOREIGN KEY  archivalReservation_ibfk_1;

UPDATE place SET avialability = 'dostępne' WHERE placeNumber NOT IN (SELECT placeNumber FROM reservation);

SELECT reservation.entryDate, reservation.placeNumber, reservation.registrationNumber, vehicle.vehicleType, vehicle.mark
FROM reservation INNER JOIN vehicle ON reservation.registrationNumber = vehicle.registrationNumber WHERE vehicle.registrationNumber LIKE '%TK%' OR mark LIKE '%Tk%';

SELECT reservation.entryDate, reservation.placeNumber, reservation.registrationNumber, vehicle.vehicleType, vehicle.mark  
				 FROM reservation INNER JOIN vehicle ON reservation.registrationNumber = vehicle.registrationNumber
				WHERE vehicle.registrationNumber LIKE '%tk%' OR mark LIKE '%tk%'

SELECT * FROM reservation

SELECT * FROM vehicle

SELECT * FROM place

INSERT INTO PLACE VALUES (45, 'dostępne')

SELECT placeNumber FROM place WHERE avialability = 'niedostępne'

